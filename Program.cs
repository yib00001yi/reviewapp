using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace reviewapp {
  public class Program {
    public static void Main (string[] args) {
      BuildWebHost (args).Run ();
    }

    public static IWebHost BuildWebHost (string[] args) =>
      WebHost.CreateDefaultBuilder (args)
      .UseStartup<Startup> ()
      .Build ();

    public static bool IsLogged (int? userId) {
      return (userId != null || userId != 0);
    }

    public static string GetSwcSHA1 (string value) {
      SHA1 algorithm = SHA1.Create ();
      byte[] data = algorithm.ComputeHash (Encoding.UTF8.GetBytes (value));
      string sh1 = "";
      for (int i = 0; i < data.Length; i++) {
        sh1 += data[i].ToString ("x2").ToUpperInvariant ();
      }
      return sh1;
    }
  }
}

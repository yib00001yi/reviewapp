using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace reviewapp.Migrations {
  public partial class AddRatingToReviewItems : Migration {
    protected override void Up(MigrationBuilder migrationBuilder) {
      migrationBuilder.AddColumn<int>(
        name: "Rating",
        table: "ReviewItems",
        nullable: false,
        defaultValue: 1);
    }

    protected override void Down(MigrationBuilder migrationBuilder) {
      migrationBuilder.DropColumn(name: "Rating", table: "ReviewItems");
    }
  }
}

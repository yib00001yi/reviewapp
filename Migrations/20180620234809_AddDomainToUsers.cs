using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace reviewapp.Migrations {
  public partial class AddDomainToUsers : Migration {
    protected override void Up(MigrationBuilder migrationBuilder) {
      migrationBuilder.AddColumn<string>(
        name: "Domain",
        table: "Users",
        nullable: false,
        defaultValue: "");
    }

    protected override void Down(MigrationBuilder migrationBuilder) {
      migrationBuilder.DropColumn(name: "Domain", table: "ReviewItems");
    }
  }
}

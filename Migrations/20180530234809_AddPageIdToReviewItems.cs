using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace reviewapp.Migrations {
  public partial class AddPageIdToReviewItems : Migration {
    protected override void Up(MigrationBuilder migrationBuilder) {
      migrationBuilder.AddColumn<string>(
        name: "PageId",
        table: "ReviewItems",
        nullable: false,
        defaultValue: "");
    }

    protected override void Down(MigrationBuilder migrationBuilder) {
      migrationBuilder.DropColumn(name: "PageId", table: "ReviewItems");
    }
  }
}

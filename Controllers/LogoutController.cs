using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using reviewapp.Models;

namespace reviewapp.Controllers {
  public class LogoutController : Controller {
    private readonly ReviewContext _context;

    public LogoutController (ReviewContext context) {
      _context = context;
    }

    public IActionResult Index () {
      HttpContext.Session.Clear ();

      return Redirect ("/");
    }
  }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using reviewapp.Models;

namespace reviewapp.Controllers {
  [Route ("api/[controller]")]
  public class RatingController : Controller {
    private readonly ReviewContext _context;

    public RatingController (ReviewContext context) {
      _context = context;
    }

    // GET /api/rating
    [HttpGet]
    public async Task<IActionResult> Index (string PageId) {
      if (PageId == null) {
        return BadRequest ();
      } else {
        var items = await (from i in _context.ReviewItems
          where i.PageId == PageId
          select i).ToListAsync();

        var count = _context.ReviewItems.Count(i => i.PageId == PageId);

        var ratingOne   = 0.0;
        var ratingTwo   = 0.0;
        var ratingThree = 0.0;
        var ratingFour  = 0.0;
        var ratingFive  = 0.0;

        foreach (ReviewItem item in items) {
          switch (item.Rating) {
            case 1: ratingOne   += 1; break;
            case 2: ratingTwo   += 1; break;
            case 3: ratingThree += 1; break;
            case 4: ratingFour  += 1; break;
            case 5: ratingFive  += 1; break;
          }
        }

        var finalRating = ( (ratingOne * 1) + (ratingTwo * 2) + (ratingThree * 3) + (ratingFour * 4) + (ratingFive * 5) );

        return Ok (finalRating / count);
      }
    }
  }
}

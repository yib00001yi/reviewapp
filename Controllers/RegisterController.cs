using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography;
using System.Text;
using reviewapp.Models;

namespace reviewapp.Controllers {
  public class RegisterController : Controller {
    private readonly ReviewContext _context;

    public RegisterController (ReviewContext context) {
      _context = context;
    }

    [HttpGet]
    public ViewResult Index () {
      ViewData["CurrentUser"] = "";
      ViewData["Title"] = "Register";

      return View ();
    }

    [HttpPost]
    public async Task<IActionResult> Index ([Bind("Name,Email,Domain,Password")]User user ) {
      ViewData["CurrentUser"] = "";
      ViewData["Title"] = "Register";

      if (user == null) {
        throw new System.ArgumentNullException (nameof (user));
      }

      try {
        if (ModelState.IsValid) {

          user.Password = reviewapp.Program.GetSwcSHA1(user.Password);

          _context.Users.Add (user);
          await _context.SaveChangesAsync ();

          return Redirect ("/");
        }
      } catch {
        ModelState.AddModelError ("", "Unable to save changes.");
        user.Password = "";
      }

      return View (user);
    }
  }
}

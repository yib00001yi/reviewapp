using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using reviewapp.Models;

namespace reviewapp.Controllers {
  public class ModerateController : Controller {

    private readonly ReviewContext _context;

    public ModerateController (ReviewContext context) {
      _context = context;
    }

    public async Task<IActionResult> Index (string PageId) {
      ViewData["CurrentUser"] = "";
      ViewData["Title"] = "Moderate";

      var userId = HttpContext.Session.GetInt32 ("UserId");
      if (reviewapp.Program.IsLogged (userId)) {
        var user = _context.Users.FirstOrDefault (b => b.Id == userId);

        if(user != null) {
          ViewData["CurrentUser"] = user.Name;
          ViewData["CurrentDomain"] = user.Domain;
          ViewData.Model = await (from i in _context.ReviewItems
            where i.PageId.StartsWith(user.Domain)
            select i)
            .OrderByDescending(i => i.Id).ToListAsync();

          return View ();
        }
      }

      return Redirect ("/Login");
    }

    public async Task<IActionResult> Hide (int ID) {
      var review = await _context.ReviewItems.FirstOrDefaultAsync (b => b.Id == ID);
      review.Visible = false;
      _context.SaveChanges();
      return RedirectToAction ("Index");
    }

    public async Task<IActionResult> Show (int ID) {
      var review = await _context.ReviewItems.FirstOrDefaultAsync (b => b.Id == ID);
      review.Visible = true;
      _context.SaveChanges();
      return RedirectToAction ("Index");
    }
  }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using reviewapp.Models;

namespace reviewapp.Controllers {
  public class LoginController : Controller {
    private readonly ReviewContext _context;

    public LoginController (ReviewContext context) {
      _context = context;
    }

    [HttpGet]
    public IActionResult Index () {
      ViewData["CurrentUser"] = "";
      ViewData["Title"] = "Login";

      var userId = HttpContext.Session.GetInt32 ("UserId");

      if (reviewapp.Program.IsLogged (userId)) {
        var user = _context.Users.FirstOrDefault (b => b.Id == userId);

        if(user != null) {
          return Redirect ("/Moderate");
        }
      }

      return View ();
    }

    [HttpPost]
    public async Task<IActionResult> Index ([Bind("Email,Password")]Login login) {
      ViewData["CurrentUser"] = "";
      ViewData["Title"] = "Login";

      if (login == null) {
        throw new System.ArgumentNullException (nameof (login));
      }

      try {
        if (ModelState.IsValid) {
          var user = await _context.Users.FirstOrDefaultAsync (u => u.Email == login.Email);

          var password = reviewapp.Program.GetSwcSHA1(login.Password);

          if(user.Password == password) {
            HttpContext.Session.SetInt32 ("UserId", user.Id);
            return Redirect ("/Moderate");
          } else {
            ModelState.AddModelError ("", "Unable to save changes.");
          }
        }
      } catch {
        ModelState.AddModelError ("", "Unable to save changes.");
        login.Password = "";
      }

      return View (login);
    }
  }
}

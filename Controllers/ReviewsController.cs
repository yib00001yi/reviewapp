using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using reviewapp.Models;

namespace reviewapp.Controllers {
  [Route ("api/[controller]")]
  public class ReviewsController : Controller {
    private readonly ReviewContext _context;

    public ReviewsController (ReviewContext context) {
      _context = context;
    }

    // GET /api/reviews
    [HttpGet]
    public async Task<IActionResult> Index (string PageId) {
      if (PageId == null) {
        return BadRequest ();
      } else {
        var items = await (from i in _context.ReviewItems
          where i.PageId == PageId && i.Visible == true
          select i).OrderByDescending(i => i.Id).ToListAsync();

        return Ok (items);
      }
    }

    // POST /api/reviews { "Content": "Testing", "Visible": false }
    [HttpPost]
    public async Task<IActionResult> Create (ReviewItem review) {
      if (review == null || review.Content == null || review.PageId == null) {
        return BadRequest ();
      }

      _context.ReviewItems.Add (review);
      await _context.SaveChangesAsync ();

      return Json (review);
    }

    // GET /api/reviews/1
    [HttpGet ("{id}")]
    public async Task<IActionResult> Get (int id) {
      var review = await _context.ReviewItems.FirstOrDefaultAsync (b => b.Id == id);

      return Ok (JsonConvert.SerializeObject (review));
    }
  }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using reviewapp.Models;

namespace reviewapp.Controllers {
  public class HomeController : Controller {

    private readonly ReviewContext _context;

    public HomeController (ReviewContext context) {
      _context = context;
    }

    public IActionResult Index () {
      ViewData["CurrentUser"] = "";
      ViewData["Title"] = "Welcome";

      var userId = HttpContext.Session.GetInt32 ("UserId");

      if (reviewapp.Program.IsLogged (userId)) {
        var user = _context.Users.FirstOrDefault (b => b.Id == userId);

        if(user != null) {
          ViewData["CurrentUser"] = user.Name;
        }
      }

      return View ();
    }
  }
}

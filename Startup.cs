using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using reviewapp.Models;

namespace reviewapp {
  public class Startup {
    public Startup (IConfiguration configuration) {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services
    // to the container.
    public void ConfigureServices (IServiceCollection services) {
      services.AddSession(options =>
          {
          options.Cookie.Name = ".reviewapp.Session";
          options.IdleTimeout = TimeSpan.FromSeconds(1200);
          });
      services.AddMvc ();

      var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

      if(env == "Production") {
        var databaseUrl = Environment
          .GetEnvironmentVariable("DATABASE_URL")
          .Replace("postgres://", string.Empty);

        var pgUserPass   = databaseUrl.Split("@")[0];
        var pgHostPortDb = databaseUrl.Split("@")[1];
        var pgHostPort   = pgHostPortDb.Split("/")[0];
        var pgDb         = pgHostPortDb.Split("/")[1];
        var pgUser       = pgUserPass.Split(":")[0];
        var pgPass       = pgUserPass.Split(":")[1];
        var pgHost       = pgHostPort.Split(":")[0];
        var pgPort       = pgHostPort.Split(":")[1];

        var conn = $"Host={pgHost};Username={pgUser};Password={pgPass};Database={pgDb};Pooling=true;Use SSL Stream=True;SSL Mode=Require;TrustServerCertificate=True;";
        services.AddDbContext<ReviewContext> (options => options.UseNpgsql(conn));
      } else {
        services.AddDbContext<ReviewContext> (options => options.UseSqlite ("Data Source=Database.db"));
      }
    }

    // This method gets called by the runtime. Use this method to configure
    // the HTTP request pipeline.
    public void Configure (IApplicationBuilder app, IHostingEnvironment env) {
      if (env.IsDevelopment ()) {
        app.UseDeveloperExceptionPage ();
      }

      app.UseCors(x => x.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials());
      app.UseStaticFiles();
      app.UseSession();
      app.UseMvc (routes =>
          {
          routes.MapRoute(
              name: "default",
              template: "{controller=Home}/{action=Index}/{id?}"
              );
          }
          );
    }
  }
}

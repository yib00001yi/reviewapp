from microsoft/dotnet:2.1-sdk

copy . /app
workdir /app
run dotnet restore
cmd ASPNETCORE_URLS=http://*:$PORT dotnet run 

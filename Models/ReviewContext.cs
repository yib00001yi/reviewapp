using Microsoft.EntityFrameworkCore;

namespace reviewapp.Models {
  public class ReviewContext : DbContext {
    public ReviewContext (DbContextOptions<ReviewContext> options) : base (options) { }

    public DbSet<reviewapp.Models.ReviewItem> ReviewItems { get; set; }
    public DbSet<reviewapp.Models.User> Users { get; set; }
  }
}

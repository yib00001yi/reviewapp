namespace reviewapp.Models {
  public class ReviewItem {
    public int Id { get; set; }
    public string Content { get; set; }
    public bool Visible { get; set; }
    public string PageId { get; set; }
    public int Rating { get; set; }
  }
}

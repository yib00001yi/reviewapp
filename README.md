# reviewapp

## Clone and Install the dependencies

```terminal
$ git clone https://dmitryrck@bitbucket.org/dmitryrck/reviewapp.git
$ cd reviewapp
$ dotnet restore
```

## Running the server

```terminal
$ dotnet ef database update
$ dotnet run
```

Or:

```terminal
$ dotnet watch run
```

## Requests

```terminal
$ curl -i localhost:5000/api/reviews/1
HTTP/1.1 200 OK
Date: Thu, 17 May 2018 23:23:12 GMT
Content-Type: text/plain; charset=utf-8
Server: Kestrel
Transfer-Encoding: chunked

{"Id":1,"Content":"Text","Visible":false}
```

```terminal
$ curl -i localhost:5000/api/reviews
HTTP/1.1 200 OK
Date: Thu, 17 May 2018 23:23:37 GMT
Content-Type: application/json; charset=utf-8
Server: Kestrel
Transfer-Encoding: chunked

[{"id":1,"content":"Text","visible":false}, {"id":2,"content":"Text#2","visible":false}]
```

```terminal
$ curl -i -H "Content-Type: application/json" --data '{"Content":"Text2"}' localhost:5000/api/reviews
```

## Html Views

Take a look at:

* `Controllers/HelloWorldController.cs`;
* `Views/HelloWorld/WelcomeHtml.cshtml`;
* `Views/Shared/_Layout.cshtml`;
* `Views/_ViewStart.cshtml`;

To test the views:

```terminal
$ curl 'localhost:5000/HelloWorld/WelcomeHtml/123?name=john&times=2'
```

Or go to [http://localhost:5000/HelloWorld/WelcomeHtml/345?name=john&times=3](http://localhost:5000/HelloWorld/WelcomeHtml/345?name=john&times=3).

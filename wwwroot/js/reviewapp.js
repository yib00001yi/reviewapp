(function() {
  var rDomain = "//secret-crag-71118.herokuapp.com"

  var templateJS = document.createElement("script")
  templateJS.src = rDomain + "/js/mustache.js"
  templateJS.type = "text/javascript"
  document.getElementsByTagName("head")[0].appendChild(templateJS)

  var mainJS = document.createElement("script")
  mainJS.src = rDomain + "/js/jquery.min.js"
  mainJS.type = "text/javascript"
  mainJS.onload = function() {
    var dollar = window.jQuery

    var pageId = window.location.href.replace(/http(s?):\/\//, "")
    var templateCommentId = "#reviewapp-comment"
    var templateComment = ""
    if((foundTemplate = dollar(templateCommentId).html()) === undefined) {
      templateComment = "<p>Rating: {{ rating }}<br>{{ content }}</p>"
    } else {
      templateComment = foundTemplate
    }
    var templateFormId = "#reviewapp-form"
    var templateForm = ""
    if((foundTemplate = dollar(templateFormId).html()) === undefined) {
      templateForm = '<input type="radio" id="review1" name="rating" value="1"> <label for="review1">1</label><br> <input type="radio" id="review2" name="rating" value="2"> <label for="review2">2</label><br> <input type="radio" id="review3" name="rating" value="3"> <label for="review3">3</label><br> <input type="radio" id="review4" name="rating" value="4"> <label for="review4">4</label><br> <input type="radio" id="review5" name="rating" value="5"> <label for="review5">5</label><br> <textarea></textarea><button>Review</button><div id="list"></div>'
    } else {
      templateForm = foundTemplate
    }

    var renderReview = templateForm

    dollar.getJSON(
      rDomain + "/api/reviews?PageId=" + pageId,
      function(items) {
        dollar.each(items, function(idx, item) {
          renderReview += Mustache.render(templateComment, item)
        })

        dollar("#reviewapp").append(renderReview)
        dollar("#reviewapp")
          .find("button")
          .click(function(event) {
            event.preventDefault()
            var textarea = dollar("#reviewapp").find("textarea")
            var rating = dollar("[name=rating]:checked").val()
            dollar.ajax({
              type: "POST",
              url: rDomain + "/api/reviews",
              dataType: "application/json",
              data: { "Content": textarea.val(), "PageId": pageId, "Rating": rating }
            })
            .always(function(response) {
              if(response.status) {
                dollar("#list").append(Mustache.render(templateComment, JSON.parse(response.responseText)))
                textarea.val('')
              }
            })
          })
      })
  }
  document.getElementsByTagName("head")[0].appendChild(mainJS)
})();
